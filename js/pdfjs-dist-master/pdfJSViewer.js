/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function pdfJSViewer(_url, canvasElement, _scale) {
    if(!_scale){
        _scale=1;
    }
    var pdfDoc = null
            , pageNum = 1
            , pageRendering = false
            , pageNumPending = null
            , scale = _scale
            , canvas = canvasElement
            , ctx = canvas.getContext('2d')
            , url = _url;
    var eventCallbacks = {};

    function addEventListener(eventName, callback) {
        if (!eventCallbacks[eventName]) {
            eventCallbacks[eventName] = new Array();
        }
        eventCallbacks[eventName].push(callback);
    }
    
    function fireEvent(eventName, args){
        if(eventCallbacks[eventName]){
            for(var i in eventCallbacks[eventName]){
                eventCallbacks[eventName][i](args);
            }
        }
    }

    function renderPage(num) {
        pageRendering = true;
        // Using promise to fetch the page
        pdfDoc.getPage(num).then(function (page) {
            var viewport = page.getViewport(scale);
            canvas.height = viewport.height;
            canvas.width = viewport.width;
            // Render PDF page into canvas context
            var renderContext = {
                canvasContext: ctx
                , viewport: viewport
            };
            var renderTask = page.render(renderContext);
            // Wait for rendering to finish
            renderTask.promise.then(function () {
                pageRendering = false;
                if (pageNumPending !== null) {
                    // New page rendering is pending
                    renderPage(pageNumPending);
                    pageNumPending = null;
                }
            });
        });
        // Update page counters
        document.getElementById('page_num').textContent = pageNum;
    }
    /**
     * If another page rendering in progress, waits until the rendering is
     * finised. Otherwise, executes rendering immediately.
     */
    function queueRenderPage(num) {
        if (pageRendering) {
            pageNumPending = num;
        } else {
            renderPage(num);
        }
    }
    /**
     * Displays previous page.
     */
    function onPrevPage() {
        if (pageNum <= 1) {
            return;
        }
        pageNum--;
        queueRenderPage(pageNum);
        fireEvent('pageChange');
    }
    document.getElementById('prev').addEventListener('click', onPrevPage);
    /**
     * Displays next page.
     */
    function onNextPage() {
        if (pageNum >= pdfDoc.numPages) {
            return;
        }
        pageNum++;
        queueRenderPage(pageNum);
        fireEvent('pageChange');
    }
    
    function getPage(){
        return pageNum;
    }

    this.showPrevPage = onPrevPage;
    this.showNextPage = onNextPage;
    this.getNumberOfPages = function () {
        return  pdfDoc.numPages;
    };
    this.getPage=getPage;
    this.addEventListener=addEventListener;
    //document.getElementById('next').addEventListener('click', onNextPage);
    /**
     * Asynchronously downloads PDF.
     */
    PDFJS.getDocument(url).then(function (pdfDoc_) {
        pdfDoc = pdfDoc_;
        document.getElementById('page_count').textContent = pdfDoc.numPages;
        // Initial/first page rendering
        renderPage(pageNum);
    });

    return this;
}

