function getlevel(callback) {
    $.ajax("http://imsofa.rocks/NSCMathTool2/apis/assessAttentionLevel.php", {
        data: {},
        success: function(data) {
            console.log(data);
            callback(data);
        }

    });
}

function gettoken(callback) {
    $.ajax("http://imsofa.rocks/NSCMathTool2/apis/getActiveLogins.php", {
        data: {},
        async: false,
        success: function(data) {
            callback(data);
        }
    });
}

function callbackFunction() {
    var token;
    var name;
    var high = 0;
    var low = 0;
   
    gettoken(function(data) {
        token = JSON.parse(data);
        //console.log(token);
    });
    
    if(!token){
        return;
    }
    getlevel(function(data) {
        var data = JSON.parse(data);
        //console.log(data);

        for (var i in data.results) {
             var tr = document.createElement("tr");
            name = token.tokens[i];
            console.log(name);
            if(!name || name=="null" || name==null){
                //name=token.tokens[i];
                name=i;
            }
            var array = data.results[i];
            if (array.length !=0) {
                for (var j in array) {
                    var assessResult = array[j];
                    if (assessResult.level == -1) {
                        high++;
                    }
                    else if (assessResult.level == 1) {
                        low++;
                    }
                }
                if (high > low) {
                    $(tr).html("<td width=30%><img src=https://cdn3.iconfinder.com/data/icons/developerkit/png/Ball%20Green.png width=20height=20></td><td align=center colspan=8>" + name + "</td>");
                    $("#leveltable").append(tr);
              
                }
                else if(low>high) {
                    $(tr).html("<td width=30%><img src=https://cdn3.iconfinder.com/data/icons/developerkit/png/Ball%20Red.png width=20height=20></td><td align=center colspan=8>" + name + "</td>");
                    $("#leveltable").append(tr);
                }else{
                     $(tr).html("<td width=30%><img src=http://imgm.photophoto.cn/023/094/005/0940052053.jpg width=20height=20></td><td align=center colspan=8>" + name + "</td>");
                     $("#leveltable").append(tr);
                }
                high = 0;
                low = 0;
            }
        }



    });

}
