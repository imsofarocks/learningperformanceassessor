/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Array.prototype.clear = function () {
    while (this.length) {
        this.pop();
    }
};

function checkCookiePresent(cookieName, redirectURL) {
    if (!Cookies.get(cookieName)) {
        window.location.href = redirectURL;
    }
}

function saveReadingLogs(logs, successCallback, errorCallback) {
    $.ajax('http://imsofa.rocks/NSCMathTool2/apis/saveReadingLogs.php', {
        'type': 'POST',
        async: false,
        'data': {
            'logs': JSON.stringify(logs),
            'key': 'abc2612'
        },
        error: function (e) {
            if(errorCallback){
                errorCallback(e);
            }
            console.log(e);
        },
        success: function (data) {
            if(successCallback){
                successCallback(data);
            }
            console.log(data);
        }
    });
    logs.clear();
}