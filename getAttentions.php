<html>

<head>
    <title>
        Attention Prompt
    </title>
    <!--meta http-equiv="refresh" content="30"-->
    <script src="https://code.jquery.com/jquery-2.2.4.js"></script>
    <script src="js/getlevel.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            callbackFunction();
        });
        var timer = window.setInterval(function() {
            $("table").find("tr").remove();
            callbackFunction();
        }, 60000);
    </script>
</head>

<body align=center>
    <div>
        <table style="border:3px #cccccc solid;"  align=center bgcolor=#FAFAFA width=10% cellpadding="10">
        <tbody id="leveltable">
            
        </tbody>
        </table>
        <h4><img src="https://cdn3.iconfinder.com/data/icons/developerkit/png/Ball%20Green.png" width=15 height=15>Attentive &nbsp;
        <img src="https://cdn3.iconfinder.com/data/icons/developerkit/png/Ball%20Red.png" width=15 height=15>Unattentive
        </h4>
    </div>
</body>

</html>
