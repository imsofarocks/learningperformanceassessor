
<html dir="ltr" lang="en-US">
    <head>
        <title>WebODF Slides Viewing Demo</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
        <link href="../css/demo-viewer.css" rel="stylesheet" type="text/css"/>
        <script src="../js/webodf.js" type="text/javascript"></script>
        <script src="../js/common.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
         <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/demo-viewer.js" type="text/javascript"></script>
        <script src="../js/js.cookie.js" type="text/javascript"></script>
        <script>
            $(document).ready(function(){
                checkCookiePresent("sid", "../index.php");
            });
            var logs=new Array();
            $(document).ready(function () {
                var cookiename = document.cookie;
                var aaa = cookiename.split('=');
                var userID = aaa[1];

                $("span").html(userID);
                console.log(userID);

                $("#save").click(function () {
                    saveReadingLogs(logs);
                });
            });

        </script>
    </head>

    <body onload="loadDocument('<?php echo $_REQUEST['file']; ?>', logs);">
        <div class="all_view">
            <div id = "viewer">
                <div id = "titlebar">
                    <div id = "toolbarLeft">
                        <div id = "navButtons" class = "splitToolbarButton">
                            <button id = "previous" class = "toolbarButton pageUp" title = "Previous Page"></button>
                            <div class="splitToolbarButtonSeparator"></div>
                            <button id = "next" class = "toolbarButton pageDown" title = "Next Page"></button>
                        </div>
                        <label id = "pageNumberLabel" class = "toolbarLabel" for = "pageNumber">Page:</label>
                        <input type = "number" id = "pageNumber" class = "toolbarField pageNumber"/>
                        <span id = "numPages" class = "toolbarLabel"></span>
                    </div>
                    <div id = "toolbarRight">
                        <h1>ID:<span id="userId" >xxx</span></h1>
                        <button id='save'>Save</button>
                    </div>
                </div>
                <div id = "toolbarContainer">
                    <div id = "toolbar">
                        <div id = "toolbarMiddleContainer" class = "outerCenter">
                            <div id = "toolbarMiddle" class = "innerCenter">
                                <div id = 'zoomButtons' class = "splitToolbarButton">
                                    <button id = "zoomOut" class = "toolbarButton zoomOut" title = "Zoom Out"></button>
                                    <div class="splitToolbarButtonSeparator"></div>
                                    <button id = "zoomIn" class = "toolbarButton zoomIn" title = "Zoom In"></button>
                                </div>
                                <span id="scaleSelectContainer" class="dropdownToolbarButton">
                                    <select id="scaleSelect" title="Zoom" oncontextmenu="return false;">
                                        <option id="pageAutoOption" value="auto" selected>Automatic</option>
                                        <option id="pageActualOption" value="page-actual">Actual Size</option>
                                        <option id="pageWidthOption" value="page-width">Full Width</option>
                                        <option id="customScaleOption" value="custom"></option>
                                        <option value="0.5">50%</option>
                                        <option value="0.75">75%</option>
                                        <option value="1">100%</option>
                                        <option value="1.25">125%</option>
                                        <option value="1.5">150%</option>
                                        <option value="2">200%</option>
                                    </select>
                                </span>
                                <div id = "sliderContainer">
                                    <div id = "slider"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id = "canvasContainer">
                    <div id = "canvas"></div>
                </div>
                <div id = "overlayNavigator">
                    <div id = "previousPage"></div>
                    <div id = "nextPage"></div>
                </div>
                <div id = "overlayCloseButton">
                    &#10006;
                </div>
            </div>
        </div>   
    </body>
</html>
