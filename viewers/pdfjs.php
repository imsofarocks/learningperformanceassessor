<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Previous/Next example</title>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="../js/pdfjs-dist-master/build/pdf.js"></script>
    <script src="../js/common.js" type="text/javascript"></script>
    <script src="../js/pdfjs-dist-master/pdfJSViewer.js" type="text/javascript"></script>
    <script src="../js/js.cookie.js" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
                checkCookiePresent("sid", "../index.php");
            });
        var logs=new Array();
        function save(){
            saveReadingLogs(logs);
        }
        $(window).on('beforeunload', function () {
            save();
        });
    </script>
</head>

<body>
    <div>
        <button id="save" onclick="save();" class="btn btn-primary" >save</button>
        <button id="prev" onclick="pdfJSViewer.showPrevPage();" class="btn btn-primary" >Previous</button>
        <button id="next" onclick="pdfJSViewer.showNextPage();" class="btn btn-primary" >Next</button> &nbsp; &nbsp; <span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
    </div>
    <div style="width: 100%; height: 100%">
        <canvas id="the-canvas" style="border:1px solid black;"></canvas>
    </div>

    <script id="script">
      var pdfJSViewer= new pdfJSViewer('<?php echo $_REQUEST['file'];?>', document.getElementById("the-canvas"), 1.3);
      var cookiename = document.cookie;
    var aaa = cookiename.split('=');
    var userID = aaa[1];
      pdfJSViewer.addEventListener('pageChange', function(){
          console.log(pdfJSViewer.getPage());
          var n=pdfJSViewer.getPage();
          logs.push({
                'userId':userID,
                'position': n,
                'timeStamp': new Date().getTime()
                
            });
            
            console.log(logs);
      });
       this.showPage = function (n) {
        if (n <= 0) {
            n = 1;
        } else if (n > pages.length) {
            n = pages.length;
        }
        var date = new Date();
        var time = date.getTime();
        var userId=document.getElementById("userId").firstChild.nodeValue;
        
        
        if ( currentPage != n){
    
            var dt={
                time:"xxx",
            };
            logs.push({
                'userId': userId,
                'position': n,
                'timeStamp': time
                
            });
            
            console.log(logs);
        }
        odfCanvas.showPage(n);

        currentPage = n;
        document.getElementById('pageNumber').value = currentPage;
    };
    </script>
</body>

</html>
