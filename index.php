<html>
    <head>
        <meta charset="utf-8"/>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
         <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/index.css" rel="stylesheet" type="text/css"/>
        <script src="js/js.cookie.js" type="text/javascript"></script>
        <script type="text/javascript">

            function addstudent() {
                var sid = document.getElementById('s_id').value;
                if (sid == "") {
                    alert("請輸入你的學號");
                } else {
                    $.ajax("https://www.imsofa.rocks/NSCMathTool2/apis/login.php", {
                        data: {
                            "userId": sid,
                            "userIdAsToken": "true"
                        },
                        success: function (data) {
                            console.log(data);
                            /*var expires = new Date();
                            expires.setTime(expires.getTime() + 28800000);
                            document.cookie = "sid=" + sid + ";expires=" + expires.toGMTString();*/
                            Cookies.set("sid", sid, {expires: 1});
                            console.log("sid=" + sid);
                            window.location.href="https://la.imsofa.rocks/mainViewerFrame.php";
                        }
                    });
                }
            }
            
            $(document).ready(function(){
                <?php
                    if(isset($_REQUEST['userId'])){
                ?>
                        $("#s_id").val("<?php echo($_REQUEST['userId']);?>");
                        addstudent();
                <?php
                    }
                ?>
            });

        </script>
    </head>
    <body>        
        <!--div class="all_list">
            輸入學號：<input type="text" id="s_id" />
            <input type="button" onclick="addstudent()" value="進入課程">

        </div-->
	
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4 text-center">
					<div class="search-box">
						<div class="caption">
							<h3>請輸入學號登入</h3>
							<p></p>
						</div>
						<form action="" class="loginForm">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Full Name" id="s_id">
								<input type="button" id="submit" class="form-control" value="進入課程" onclick="addstudent()">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

    </body>
</html>
