<html>
    <head>
        <meta charset="utf-8"/>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
         <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="js/js.cookie.js"></script>
        <!--link href="css/courseList.css" rel="stylesheet" type="text/css"/-->
        <script>
			var running=true;
            function dataURLtoBlob(dataurl) {
                var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
                        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
                while (n--) {
                    u8arr[n] = bstr.charCodeAt(n);
                }
                return new Blob([u8arr], {type: mime});
            }
            
            function toggleCapturing(){
				if(running){
					running=false;
					$("#buttonToggleCapturing").attr("value", "Start");
				}else{
					running=true;
					$("#buttonToggleCapturing").attr("value", "Stop");
				}
			}
            
            $(document).ready(function(){
                var url="https://docs.google.com/forms/d/e/1FAIpQLSdGy8nrLWx4Ny2Cw_abzqczhOR-BWeX4DfCFrbO_7vK6QSVCQ/viewform?entry.1263599707=";
                url+=Cookies.get("sid");
                $("#test1").attr("href", url);
                $("#body").mouseenter(function(){
                    $(parent.document).find("frameset").attr("cols", "20%,*");
                });
                $("#body").mouseleave(function(){
                    $(parent.document).find("frameset").attr("cols", "5%,*");
                });
                
                //image capture part
                var canvas = document.getElementById("canvas");
                var context = canvas.getContext("2d");
                var video = document.getElementById("video");
                var localMediaStream = null;
                var seq=1;
                
                navigator.getUserMedia = navigator.getUserMedia ||
                        navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;

		setTimeout(function () {
                    captureImage();
                }, 5000);

	
                function captureImage() {
					if(!running){
						 setTimeout(function () {
                                captureImage();
                            }, 5000);
                            return;
					}
                    navigator.getUserMedia({video: {facingMode: "user"}}, function (stream) {
                        video.src = window.URL.createObjectURL(stream);
                        localMediaStream = stream;
                        setTimeout(function () {
                            context.drawImage(video, 0, 0, 240, 180);
                            //console.log(canvas.toDataURL("image/png"));

                           
                            var form = new FormData();
                            console.log(dataURLtoBlob(canvas.toDataURL("image/png")));
                            form.append("image", dataURLtoBlob(canvas.toDataURL("image/png")), Cookies.get("sid")+"_"+(seq++)+"_"+(new Date().getTime())+".png");

                            $.ajax("https://www.imsofa.rocks/NSCMathTool2/apis/uploadImage.php", {
                                "type": "POST",
                                "data": form,
                                contentType: false,
                                processData: false,
                                success: function (data) {
                                    console.log("success: " + data);
                                },
                                error: function (xhr) {
                                    console.log("error: " + xhr);
                                }
                            });

                            setTimeout(function () {
                                captureImage();
                            }, 5000);
                        }, 500);

                    }, function (e) {
                        console.log(e);
                    });
                }
            });
        </script>
    </head>
    <body id="body">
        <video id="video" autoplay="true" width="1" height="1"></video><br/>
        <canvas id="canvas" width="240" height="180" style="display:none"></canvas><br/>
        <div class="all_list btn-group-vertical">
            <!--p><a href="viewers/webodf.php?file=../materials/ohm2013.odp" target="viewer">第一單元</a></p>
            <p><a href="viewers/video.php?file=_n7q1mAk_Mc" target="viewer">第二單元</a></p>
            <p><a href="viewers/pdfjs.php?file=../materials/WeekX.pdf" target="viewer">第三單元</a></p-->
            <a href="viewers/pdfjs.php?file=../materials/函數.pdf" target="viewer" class="btn btn-primary btn-lg" role="button">函數</a>
            <a href="viewers/pdfjs.php?file=../materials/函數的圖形.pdf" target="viewer" class="btn btn-primary btn-lg" role="button">函數的圖形</a>
            <!--p><a href="viewers/pdfjs.php?file=../materials/教育部國民中學‧學習資源網-變數與函數.pdf" target="viewer">變數與函數</a></p>
            <p><a href="viewers/pdfjs.php?file=../materials/教育部國民中學‧學習資源網-線型函數及其圖形.pdf" target="viewer">變數與函數</a></p-->
            <a href="viewers/video.php?file=_n7q1mAk_Mc" target="viewer"  class="btn btn-primary btn-lg" role="button">函數的基本(影片)</a>
            <a id="test1" href="https://docs.google.com/forms/d/e/1FAIpQLSdGy8nrLWx4Ny2Cw_abzqczhOR-BWeX4DfCFrbO_7vK6QSVCQ/viewform?entry.1263599707=abc" target="viewer"  class="btn btn-primary btn-lg" role="button">測驗</a>
            <a href="logout.html" target="viewer"  class="btn btn-primary btn-lg" role="button">登出</a>
            <input type="button"  class="btn btn-primary btn-lg" onclick="toggleCapturing()" value="Stop" id="buttonToggleCapturing"/>
        </div>
    </body>
</html>
