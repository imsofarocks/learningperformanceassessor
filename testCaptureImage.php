<html>
    <head>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script>
        		function dataURLtoBlob(dataurl) {
    				var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        			bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    				while(n--){
        				u8arr[n] = bstr.charCodeAt(n);
		    		}
    				return new Blob([u8arr], {type:mime});
				}
            
				var keepUpload=true;            
            
            $(document).ready(function () {
                var canvas = document.getElementById("canvas");
                var context = canvas.getContext("2d");
                var video = document.getElementById("video");
                var localMediaStream = null;
                navigator.getUserMedia = navigator.getUserMedia ||
                        navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
                
                navigator.mediaDevices.enumerateDevices().then(function(devices){
                    for(var i in devices){
                        if(devices[i].kind=="videoinput"){
                            console.log(devices[i].label);
                        }
                    }
                });
                
					 setTimeout(function(){
									captureImage();                        
                }, 5000);                
                
                function captureImage(){
                	  navigator.getUserMedia({video: {facingMode:"user"}}, function (stream) {
                        video.src = window.URL.createObjectURL(stream);
                        localMediaStream = stream;
                        setTimeout(function () {
                        	context.drawImage(video, 0, 0, 320, 240);
                        	//console.log(canvas.toDataURL("image/png"));
                        	
                        	if(!keepUpload){
                        		return;
                        	}
                        	var form = new FormData();
                        	console.log(dataURLtoBlob(canvas.toDataURL("image/png")));
                        	form.append("image", dataURLtoBlob(canvas.toDataURL("image/png")), "20170607.png");

                        	$.ajax("https://www.imsofa.rocks/NSCMathTool2/apis/uploadImage.php", {
										"type": "POST",
										"data": form,
										contentType: false,
										processData: false,
										success: function(data){
											console.log("success: "+data);
										},
										error: function(xhr){
											console.log("error: "+xhr);
										}                        	
                        	});
                        	
                        	setTimeout(function(){
										captureImage();                        
                        	}, 5000);	
                        }, 500);
                        
                    }, function (e) {
                        console.log(e);
                    });
                }
                
                
                $("#buttonCapture").click(function () {
                    navigator.getUserMedia({video: {facingMode:"user"}}, function (stream) {
                        video.src = window.URL.createObjectURL(stream);
                        localMediaStream = stream;
                    }, function (e) {
                        console.log(e);
                    });
                });
                
                $("#buttonDraw").click(function () {
                    context.drawImage(video, 0, 0, 640, 480);
                });

                $("#buttonStopCapture").click(function () {
                	  keepUpload=false;
                    video.pause();
                    localMediaStream.stop();
                });
            });
        </script>
    </head>
    <body>
        <video id="video" autoplay="true" width="1" height="1"></video><br/>
        <canvas id="canvas" width="240" height="180"></canvas><br/>
        <input type="button" id="buttonCapture" value="capture"/>
        <input type="button" id="buttonDraw" value="draw"/>
        <input type="button" id="buttonStopCapture" value="stopCapture"/>
    </body>
</html>
